from django.urls import path
from .views import index, message_post, message_table

urlpatterns = [
    path('result_table', message_table, name='result_table'),

]
