from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message


def index(request):
    response['message_form'] = Message_Form
    return render(request, html, response)


def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(
            name=response['name'], email=response['email'], message=response['message'])
        message.save()
        html = 'form/form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/')
